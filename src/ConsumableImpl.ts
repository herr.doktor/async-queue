import { Consumable } from './Consumable';

export class ConsumableImpl<T> implements Consumable<T> {
    private terminated = false;

    constructor(
        private readonly item: T,
        private readonly rejectCallback: (item: T) => void
    ) { }

    private hasBeenTerminated(): boolean {
        return this.terminated;
    }

    private checkIsValid() {
        if (this.hasBeenTerminated()) {
            throw new Error('Element already consumed or rejected');
        }
    }

    private markTerminated() {
        this.checkIsValid();
        this.terminated = true;
    }

    public consume(): void {
        this.markTerminated();
    }

    public reject(): void {
        this.markTerminated();
        this.rejectCallback(this.item);
    }

    public getContent(): T {
        return this.item;
    }

}
