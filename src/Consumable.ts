export interface Consumable<T> {

    consume(): void;

    reject(): void;

    getContent(): T;

}
