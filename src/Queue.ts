import { Consumable } from "./Consumable";
import { ConsumableImpl } from './ConsumableImpl';

type ItemConsumer<T> = (item: T) => void;
type OnEmptyCallbackType<T> = (queue: Queue<T>) => void;

export class Queue<T> {
    private readonly items: Array<T> = [];
    private readonly pendingConsumers: Array<ItemConsumer<T>> = [];
    private onEmptyCallback: OnEmptyCallbackType<T> = () => {};
    
    private queueIsEmpty(): boolean {
        return this.items.length === 0;
    }

    private thereArePendingConsumers(): boolean {
        return this.pendingConsumers.length > 0;
    }

    private wakeUpConsumer(item: T): void {
        const consumer: (item: T) => void = this.pendingConsumers.shift()!;

        setImmediate(() => {
            consumer(item);
            this.invokeOnEmptyCallback();
        });
    }

    private enqueueConsumer(): Promise<T> {
        return new Promise((resolve) => {
            this.pendingConsumers.push(resolve);
        });
    }

    private enqueueItem(newItem: T): void {
        this.items.push(newItem);
    }

    private dequeueItem(): T {
        const ret = this.items.shift();

        if (this.queueIsEmpty()) {
            setImmediate(() => this.invokeOnEmptyCallback());
        }

        return ret!;
    }

    private invokeOnEmptyCallback(): void {
        this.onEmptyCallback(this);
    }

    private pushFront(newItem: T): void {
        if (this.thereArePendingConsumers()) {
            this.wakeUpConsumer(newItem);
        } else {
            this.items.unshift(newItem);
        }
    }

    public push(newItem: T): void {
        if (this.thereArePendingConsumers()) {
            this.wakeUpConsumer(newItem);
        } else {
            this.enqueueItem(newItem);
        }
    }

    /**
     * Extracts an item from the queue.
     * If no item is available, the promise is held until a new item is pushed into the queue.
     */
    public async pop(): Promise<Consumable<T>> {
        return (this.queueIsEmpty() || this.thereArePendingConsumers() ?
            this.enqueueConsumer() :
            Promise.resolve(this.dequeueItem())
        ).then((item) => new ConsumableImpl(item, (item) => this.pushFront(item)));
    }

    public length(): number {
        return this.items.length;
    }

    /**
     * Registers a callback to be invoked as soon as the queue becomes empty.
     * When the queue becomes empty (i.e.: the last item has been removed), the callback is invoked and he queue is passed as an argument.
     * The callback is run asynchronously, hence the queue could be no longer empty the when the callback is run.
     *
     * @param callback
     */
    public onEmpty(callback: OnEmptyCallbackType<T>): void {
        this.onEmptyCallback = callback;
    }

}
